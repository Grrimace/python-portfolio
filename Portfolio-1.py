from random import randint
import emoji


print("This program is called \"List Gymnastics\"")
print("Let's see what happens in the following list.\n")

alphabet = ["f","b","w","x","r","n","a","W","Z","e","i","U","s","G"]
print(str(alphabet) + "\n")

print("The alphabet list is made up of lower and upper case strings.")
print("Let's find all the upper case letters!\n")

upperCase = []
lowerCase = []

#This loops through the list and repeats a conditional that looks for
#capital letters only. Notice w and W are in the list but the lower-case
#letter is not duplicated in the final result

for alpha in alphabet:
    if alpha == alpha.upper() and alpha in alphabet:
                upperCase.append(alpha)

print("All the capital letters in the alphabet list are: ")
print(upperCase)

print("\nLet's find all the lower case letters!\n")

for alpha in alphabet:
    if alpha == alpha.lower() and alpha in alphabet:
                lowerCase.append(alpha)

print(str(lowerCase) + "\n")
print("Let's try something harder!\n" \
      + "\nLet's replace all the lower case letters with numbers!")

for i in range(len(alphabet)):
    
    if alphabet[i] == alphabet[i].lower() and alphabet[i] in alphabet:
        
        alphabet[i] = randint(1,1001)
        i += 1
        
print("Great! Now the alphabet list looks like this! \n")
print(alphabet)

print("\nLet's do something fun! You think we can replace ") 
print("the capital letters with emojis?\n")

emojiList = []


emojiList.append(emoji.emojize(":crescent_moon:"))


emojiList.append(emoji.emojize(":kiss:"))


emojiList.append(emoji.emojize(":cyclone:"))


emojiList.append(emoji.emojize(":honeybee:"))

backupList = alphabet[:]

for i in range(len(alphabet)):
    
    if str(alphabet[i]).isnumeric():
        i += 1
        continue
    
    if alphabet[i] == alphabet[i] and \
    alphabet[i] in alphabet:
        
        choiceEmoji = int(randint(0,len(emojiList) - 1))
        
        alphabet[i] = emojiList[choiceEmoji]
        
        i += 1
        

print("\nLet's take a look at our list now!\n")
print(alphabet)

